import matplotlib.pyplot as plt
%matplotlib inline


df['column'].plot.hist()

df['column'].value_counts().plot.bar()

df['column'].value_counts().plot.barh()

df['column'].value_counts().plot.pie()

df.plot.scatter(x='price', y='area')
